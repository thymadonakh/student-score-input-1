import React from 'react'
import { Typography, Box, CssBaseline } from '@mui/material'
import { Container } from '@mui/system'

export default function home() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth='fixed'>
        <Box sx={{
            height: '100vh',
            // bgcolor: '#784BA0',
            backgroundImage: 'linear-gradient(225deg, #FF3CAC 0%, #784BA0 50%, #2B86C5 100%)'
         }}>
            <Typography variant="h2" sx={{color: 'white', fontWeight:'bold', paddingTop:'100px'}}>Welcome to the platform!</Typography>
        </Box>
      </Container>
    </React.Fragment>
  )
}
