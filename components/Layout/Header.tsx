import React, {useState} from "react";
import Link from "next/link";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Stack,
  Button,
  Box,
} from "@mui/material";
// import { CatchingPokemon } from "@mui/icons-material";

export default function Header() {
  return (
    <Box>
      <AppBar
        position="static"
        sx={{
          bgcolor: "#784BA0",
          // backgroundImage: 'linear-gradient(225deg, #FF3CAC 0%, #784BA0 50%, #2B86C5 100%)'
        }}
      >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            ariel-label="logo"
          >
            {/* <CatchingPokemon /> */}
          </IconButton>

          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, cursor: "pointer" }}
          >
            <Link href="/" passHref>
              Algorithmic Phnom Penh
            </Link>
          </Typography>

          <Stack direction="row" spacing={2}>
            <Button color="inherit">
              <Link href="/" passHref>
                <a>Home</a>
              </Link>
            </Button>
            <Button color="inherit">
              <Link href="/enrollment" passHref>
                <a>Enrollment</a>
              </Link>
            </Button>
            <Button color="inherit">
              <Link href="/scoreInput" passHref>
                <a>Score Input</a>
              </Link>
            </Button>
            <Button color="inherit">
              <Link href="/login" passHref>
                <a>Login</a>
              </Link>
            </Button>
          </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
