import { Typography } from "@mui/material";
import React from "react";

function Footer() {
  return (
    <>
      <Typography
        component="div"
        sx={{ position: "fixed", bottom: 0, backgroundColor: "red" }}
      >
        Copyright © 2022s
      </Typography>
    </>
  );
}

export default Footer;
