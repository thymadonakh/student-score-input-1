import Footer from "./Footer";
import React, { Fragment } from "react";
import Header from "./Header";
import { Container } from "@mui/system";
import { Button, Typography } from "@mui/material";

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <Container>{children}</Container>
      <Footer />
    </>
  );
};

export default Layout;
