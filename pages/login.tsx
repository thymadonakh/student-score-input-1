import React from 'react'
import {
  Button,
  Container,
  Box,
  TextField,
  Stack,
  Typography,
  Checkbox
} from "@mui/material";

export default function Login() {
  function clickHandler() {
    alert("Logged in");
  }
  return (
    <div className="App">
      <Container
        maxWidth="sm"
        sx={{
          p: 5
        }}
      >
        {/* Chil components of Container */}
        <Typography variant="h3" color="white" sx={{ mt: 5 }} align="center">
          Welcome!
        </Typography>
        <Stack
          sx={{
            p: 6,
            mt: 5,
            bgcolor: "white",
            boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
            borderRadius: "10px"

          }}
          spacing={5}
        >
          <TextField
            id="outlined-basic"
            label="Email"
            placeholder="abcxyz@gmail.com"
            type="text"
            variant="outlined"
          />
          <TextField
            id="outlined-basic"
            label="Passwords"
            placeholder="Passwords"
            type="password"
            variant="outlined"
          />
          <Button
            onClick={clickHandler}
            variant="contained"
            size="medium"
            fullWidth="true"
            color="secondary"
          >
            Login
          </Button>
          <Box sx={{}}>
            <Checkbox defaultChecked />
            <Typography variant="p">Keep me logged in</Typography>
          </Box>
        </Stack>
      </Container>
    </div>
  );
}

