import { Box, Button, InputLabel, Stack, TextField } from "@mui/material";
import { validateConfig } from "next/dist/server/config-shared";
import React, { useState } from "react";
import BasicTable from "../components/Table/BasicTable";

const data = [
  { id: 1, name: "John Doe", class: "VS103-01" },
  { id: 2, name: "Ly Lily", class: "VU200-02" },
];

const Enrollment = () => {
  const [allData, setAllData] = useState(data);
  const [val, setVal] = useState({ id: "", name: "", class: "" });
  const [edited, setEdited] = useState(false);
  const [index, setIndex] = useState(null);

  const handleAdd = (id) => {
    if (edited) {
      const indexOfArray = index;
      // update the object with index, any better way?
      allData[indexOfArray].name = val.name;
      allData[indexOfArray].class = val.class;
      setAllData([...allData]);
    } else {
      if (Object.keys(val.id).length > 0 && val.name.length > 0 && val.class.length > 0) {
        const addItem = { id: val.id, name: val.name, class: val.class };
        setAllData([...allData, addItem]);
      }
    }
  };
  const handleEdit = (values) => {
    const index = allData.findIndex((item) => item.id == values.id);
    setIndex(index);
    setEdited(true);
    setVal(values);
  };
  const handleDelete = (id) => {
    var data = allData.filter((data, index) => data.id !== id);
    console.log(data);
    setAllData([...data]);
  };

  const handleClear = (e) => {
    e.preventDefault();
    setVal({ id: "", name: "", class: "" });
    setEdited(false);
  };
  const columns = [
    {
      Header: "ID",
      accessor: "id",
    },
    {
      Header: "Name",
      accessor: "name",
    },
    {
      Header: "Class Code",
      accessor: "class",
    },
    {
      Header: "ACTION",
      accessor: "action",
      Cell: ({ row }) => {
        const { values } = row;
        return (
          <>
            <Box sx={{ display: "flex", gap: 2 }}>
              <Button variant="contained" onClick={() => handleEdit(values)}>
                {" "}
                Edit
              </Button>
              <Button
                variant="contained"
                color="error"
                onClick={() => handleDelete(values.id)}
              >
                DELETE
              </Button>
            </Box>
          </>
        );
      },
    },
  ];

  return (
    <>
      <BasicTable columns={columns} data={allData} />
      {JSON.stringify(val)}
      {/* { ...val } */}

      {/* {name} */}
      <form>
        <Stack gap={2}>
          <InputLabel htmlFor="name">ID</InputLabel>
          <TextField
            autoComplete="off"
            id="id"
            variant="outlined"
            value={val.id}
            onChange={(e) => setVal({ ...val, id: e.target.value })}
          />
          <InputLabel htmlFor="name">Name</InputLabel>
          <TextField
            autoComplete="off"
            id="name"
            variant="outlined"
            value={val.name}
            onChange={(e) => setVal({ ...val, name: e.target.value })}
          />
          <InputLabel htmlFor="name">Class</InputLabel>
          <TextField
            id="class"
            variant="outlined"
            value={val.class}
            onChange={(e) => setVal({ ...val, class: e.target.value })}
          />
          <Button variant="contained" onClick={() => handleAdd(allData.id)}>
            {edited ? "Save" : "Add"}{" "}
          </Button>
          <Button variant="contained" onClick={handleClear}>
            Clear
          </Button>
        </Stack>
      </form>
    </>
  );
};

export default Enrollment;
