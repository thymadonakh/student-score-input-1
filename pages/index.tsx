import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Score Input</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://phnompenh.alg.academy/en">Algorithmics Teacher Platform</a>
        </h1>
        <h2>You can enroll students or input score here</h2>
        <p className={styles.description}>
          Get start now!
        </p>
      </main>

      <footer className={styles.footer}>
        <p>Copyright 2022</p>
      </footer>
    </div>
  )
}

export default Home
