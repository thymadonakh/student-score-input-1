// import { Typography } from '@mui/material';

// project import
import { PlusOutlined, SearchOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Fab,
  FormControl,
  InputBase,
  MenuItem,
  Select,
  Stack,
  Typography
} from '@mui/material';
import ReactTable from 'components/common/ReactTable';
// import { FormControl, InputLabel, MenuItem, TextField } from '@mui/material';
import MainCard from 'components/MainCard';
import { set } from 'lodash';
import { useEffect, useMemo, useState } from 'react';
import AddEditNotice from '../../components/notice/AddEditNotice';
import axios from 'axios';
import { format } from 'date-fns';
import { isObjEmpty } from 'utils/objectManipulation';
import request from 'api/core/axios_lms';
import useRequest from 'hooks/useRequest';

import { useDispatch, useSelector } from 'react-redux';

const SERVER_URL = `${process.env.REACT_APP_LOCAL_URL}:${process.env.REACT_APP_SERVER_POST}`;

const Notice = () => {
  const dispatch = useDispatch();
  const notice = useSelector((state) => state.notice);

  const obj_init = {
    id: null,
    courseName: '',
    subject: '',
    title: '',
    created_at: ''
  };
  const [searchVal, setSearchVal] = useState('');
  const [open, setOpen] = useState(false);
  const [edit, setEdit] = useState(false);
  const [info, setInfo] = useState(obj_init);
  const [list, setList] = useState([]);
  const [deletedID, setDeletedID] = useState(null);

  const [allSubjects, setAllSubjects] = useState([]);
  const [allCourseNames, setAllCourseNames] = useState([]);

  /** FETCH DATA */
  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      try {
        axios.get(SERVER_URL + '/api/notice').then((response) => {
          setList(response.data);
        });
      } catch (error) {
        console.log(error);
      }
    }

    return () => {
      isMounted = false;
      // clean
    };
  }, []);

  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      try {
        axios.get(SERVER_URL + '/api/notice/namelist').then((response) => {
          const res = response.data;

          setAllCourseNames(res[0]);
          setAllSubjects(res[1]);
        });
      } catch (error) {
        console.log('ERROR', error);
      }
    }

    return () => {
      isMounted = false;
    };
  }, []);

  /** FUNCTION HANDLERS */
  const handleClose = () => {
    setEdit(false);
    setOpen(false);
  };

  const handleEdit = (values) => {
    setEdit((edit) => !edit);

    /** IF THERE IS NO "INFO (DATA)", SET IT TO INITIAL STAGE */
    !isObjEmpty(info) ? setInfo(obj_init) : setInfo(values);
  };

  const handleDelete = async (id) => {
    try {
      await axios.delete(SERVER_URL + '/api/notice/' + id).then((response) => {
        deleteTableData(id);
        handleClose();
      });
    } catch (error) {
      console.log(error);
    }
  };

  /** FUNCTIONS CRUD (UPDATE STATE) */

  const addTableData = (newData) => {
    setList([newData, ...list]);
  };

  const updateTableData = (updateData) => {
    const id = updateData.id;
    const updateDatas = [...list];
    const findData = updateDatas.find((data) => data.id === id);
    findData.title = updateData.title;
    findData.content = updateData.content;
    setList(updateDatas);
  };

  const deleteTableData = (id) => {
    var newResult = list.filter((data) => data.id !== id);
    setList(newResult);
  };

  const columns = useMemo(
    () => [
      {
        Header: 'No.',
        accessor: 'id'
      },
      {
        Header: '과정이름',
        accessor: 'courseName'
      },
      {
        Header: '과목',
        accessor: 'subject'
      },
      {
        Header: '제목',
        accessor: 'title',
        className: 'cell-center'
      },
      {
        Header: '등록일자',
        accessor: 'created_at',
        className: 'cell-center'
      },
      {
        Header: '내용',
        accessor: 'content'
      },
      {
        Header: '수정',
        asccessor: 'edit',
        disableSortBy: true,
        className: 'cell-center',
        Cell: ({ row }) => {
          const { values } = row;

          return (
            <>
              <Typography sx={{ display: 'flex', gap: 2, flexWrap: 'wrap', justifyContent: 'center' }}>
                <Button
                  variant="contained"
                  onClick={(e) => {
                    e.stopPropagation();
                    handleEdit(values);
                  }}
                  sx={{ minWidth: 90 }}
                >
                  수정하기
                </Button>
                <Button
                  variant="contained"
                  color="error"
                  onClick={(e) => {
                    e.stopPropagation();
                    setOpen(true);
                    setDeletedID(values.id);
                  }}
                  sx={{ minWidth: 90 }}
                >
                  삭제하기
                </Button>
              </Typography>
            </>
          );
        }
      }
    ],
    []
  );

  list.forEach((vals, i) => {
    // console.log('VALLL', vals);
    const data = {
      changeDate: function () {
        if (this.created_at == null) return null;
        if (this.created_at !== undefined) return format(new Date(this.created_at), 'yyyy.MM.dd');
      }
    };
    var newDate = data.changeDate.apply(vals);
    vals['created_at'] = newDate;
  });

  const data = useMemo(() => list, [list]);

  const handleChange = (event) => {
    event.preventDefault();
    setSearchVal(event.target.value);
  };

  const hiddenColumns = ['content'];

  return (
    <>
      {/* DELETE */}


      {/* EDIT */}
      <Dialog open={edit} onClose={handleClose}>
        <AddEditNotice
          info={info}
          handleClose={handleClose}
          addTableData={addTableData}
          updateTableData={updateTableData}
          allSubjects={allSubjects}
          allCourseNames={allCourseNames}
        />
      </Dialog>

      <MainCard>
        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <FormControl sx={{ m: 1, minWidth: 250 }}>
            <Select
              value={searchVal}
              onChange={handleChange}
              displayEmpty
              inputProps={{ 'aria-label': 'Without label' }}
              MenuProps={{ disableScrollLock: true }}
            >
              <MenuItem value="">전체</MenuItem>
              {allCourseNames.map((vals) => {
                return (
                  <MenuItem key={vals.id} value={vals.name}>
                    {vals.name}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <Typography component="div" sx={{ display: 'flex', justifyItems: 'center', justifyContent: 'end' }}>
            <Fab color="primary" size="small" onClick={handleEdit} aria-label="add" sx={{ mr: 5 }}>
              <PlusOutlined style={{ fontSize: '1.3rem' }} />
            </Fab>
          </Typography>
        </Box>
      </MainCard>

      <MainCard component="div" sx={{ mt: '10px' }}>
        <ReactTable columns={columns} data={data} searchVal={searchVal} columnName={'courseName'} hiddenColumns={hiddenColumns} />
      </MainCard>
    </>
  );
};

Notice.propTypes = {
  'row.values.id': PropTypes.number,
  row: PropTypes.object
};
export default Notice;
